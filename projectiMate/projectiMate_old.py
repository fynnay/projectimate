'''
File : projectiMate.py
Description : Python code for the projectiMate node made to help projecting textures onto moving 3D objects in Nuke.
'''

import nuke

def getValidNode(startNode,validClasses=[],skipClasses=[]):
    '''
    Walk up the node tree until a valid node of the defined class is defined.
    First match is returned.

    startNode    : from which node to start walking
    validClasses : names of the classes to be retured
    skipClasses  : names of classes allowed to be between startNode and nodes with validClasses
    '''
    if startNode is None:
        return None
    for i in range(startNode.inputs()):
        inp = startNode.input(i)
        # Return node if its in validClasses
        if inp.Class() in validClasses:
            return inp
        # Walk up tree if node is in skipClasses
        elif inp.Class() in skipClasses:
            return getValidNode(inp,validClasses,skipClasses)
        # Cancel if the above criteria don't match
        else:
            return None

def main():
    # VARIABLES
    camClassCriteria  = ["Camera2"] # "Axis2"
    geoClassCriteria  = ["Axis2"]
    skipClassCriteria = ["Dot"]
    n   = nuke.toNode("projectiMate")#nuke.thisNode()
    refFrame = n['reference_frame'].getValue()
    camAni = n.input(0)
    geoAni = n.input(1)

    # CHECKS AND SETUP
    # Check inputs
    camAni = getValidNode(camAni,camClassCriteria,skipClassCriteria)
    if camAni is None:
        print "ERROR : No valid Camera node found."
        return -1
    geoAni = getValidNode(geoAni,geoClassCriteria,skipClassCriteria)
    if geoAni is None:
        print "ERROR : No valid GeoAni node found"
        return -1

    # CREATE PROJECTOR
    projectorCopyKnobs = ["translate","rotate","scaling","uniform_scale","skew","pivot","focal","haperture","vaperture","near","far","win_translate","win_scale","winroll","focal_point","fstop"]
    projector = nuke.nodes.Camera2()
    projector['label'].setValue("projector\nframe {0}".format(int(refFrame)))
    # Copy values at reference frame from camera to projector
    for i in projectorCopyKnobs:
        projector[i].setValue(camAni[i].getValueAt(refFrame))

    # CREATE CENTERGEO
    centerGeo = nuke.nodes.Axis2()
    centerGeo['label'].setValue("center geo\nframe {0}".format(int(refFrame)))
    centerGeoCopyKnobs = ["translate","rotate","scaling","uniform_scale","skew","pivot"]
    # Set centerGeo's values to be the inverted values of geoAni at the reference frame
    for i in centerGeoCopyKnobs:
        val = geoAni[i].getValueAt(refFrame)
        if isinstance(val,list):
            centerGeo[i].setValue([i*-1 for i in val])
        else:
            centerGeo[i].setValue(val*-1)
    # Set inverted transform and rotation order
    centerGeo["xform_order"].setValue(geoAni["xform_order"].value()[::-1])
    centerGeo["rot_order"].setValue(geoAni["rot_order"].value()[::-1])

    # CREATE CHEKCERBOARD AND POSTAGESTAMP
    checkerBoard = nuke.nodes.CheckerBoard2()
    pStampTex = nuke.nodes.PostageStamp()
    pStampTex['label'].setValue("texture")

    # CREATE PROJECT 3D
    project3D = nuke.nodes.Project3D2()

    # CREATE SCANLINE RENDER
    scanlineRender = nuke.nodes.ScanlineRender()

    # CONNECT NODES
    centerGeo.setInput(0,geoAni)
    projector.setInput(0,centerGeo)
    pStampTex.setInput(0,checkerBoard)
    project3D.setInput(0,pStampTex)
    project3D.setInput(1,projector)

    # ORGANIZE NODES



if __name__ == '__main__':
    main()
