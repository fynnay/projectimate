def main():
    import nuke
    import os

    # Strings
    parentDir  = os.path.dirname(__file__)
    menuStr    = os.path.basename(parentDir)

    # Menus
    menu  = nuke.menu("Nodes").addMenu(menuStr)
    spprtdExt = ["nk","gizmo"]
    files = sorted( [i for i in os.listdir(parentDir) if not i.startswith(".") and i.split(".")[1] in spprtdExt] )
    for i in files:
        name  = "".join(i.split(".")[:-1])
        cmd   = menu.addCommand(name,lambda: nuke.createNode('%s'%i))
main()