'''
File : projectiMate.py
Description : Python code for the projectiMate node made to help projecting textures onto moving 3D objects in Nuke.
'''

import nuke

def getValidNode(startNode,validClasses=[],skipClasses=[]):
    '''
    Walk up the node tree until a valid node of the defined class is defined.
    First match is returned.

    startNode    : from which node to start walking
    validClasses : names of the classes to be retured
    skipClasses  : names of classes allowed to be between startNode and nodes with validClasses
    '''
    if startNode is None:
        return None
    elif startNode.Class() in validClasses:
        return startNode
    for i in range(startNode.inputs()):
        inp = startNode.input(i)
        # Return node if its in validClasses
        if inp.Class() in validClasses:
            return inp
        # Walk up tree if node is in skipClasses
        elif inp.Class() in skipClasses:
            return getValidNode(inp,validClasses,skipClasses)
        # Cancel if the above criteria don't match
        else:
            return None

def main():
    # VARIABLES
    camClassCriteria  = ["Camera2"] # "Axis2"
    geoClassCriteria  = ["Axis2"]
    skipClassCriteria = ["Dot"]
    n = nuke.thisNode()
    with n as n:
        refFrame = n['reference_frame'].getValue()
        camAniSource = n.input(0)
        camAniDestin = nuke.toNode("SOURCE_CAM")
        geoAniSource = n.input(1)
        geoAniDestin = nuke.toNode("SOURCE_GEOANI")
        centerGeo    = nuke.toNode("CENTER_GEO")
        statusKnob   = n['status']
        statusMsg    = ""

        # CHECK INPUTS
        camAniSource = getValidNode(camAniSource,camClassCriteria,skipClassCriteria)
        if camAniSource is None:
            #nuke.message("ERROR : No valid Camera node found.")
            statusMsg += '<font color="red">Invalid cam input'
        geoAniSource = getValidNode(geoAniSource,geoClassCriteria,skipClassCriteria)
        if geoAniSource is None:
            #nuke.message("ERROR : No valid geoAniSource node found")
            statusMsg += '<font color="red">Invalid geoAni input'
        # Update statusMsg
        if statusMsg == "":
            statusMsg = '<font color="green">Good'
            statusKnob.setValue(statusMsg)
        else:
            statusKnob.setValue(statusMsg)
            return -1

        # CAM SOURCE
        # Copy animation curve from camAniSource to camAniDestin
        camAniCopyKnobs = ["xform_order","rot_order","translate","rotate","scaling","uniform_scale","skew","pivot","focal","haperture","vaperture","near","far","win_translate","win_scale","winroll","focal_point","fstop"]
        for i in camAniCopyKnobs:
            camAniDestin[i].clearAnimated()
            # Copy animation if one exists
            if camAniSource[i].isAnimated():
                camAniDestin[i].copyAnimations(camAniSource[i].animations())
            # Otherwise just copy the values
            else:
                try:
                    camAniDestin[i].setValue(camAniSource[i].getValue())
                except TypeError:
                    camAniDestin[i].setValue(camAniSource[i].value())

        # UPDATE PROJECTOR
        projectorCopyKnobs = ["xform_order","rot_order"]
        for i in projectorCopyKnobs:
            camAniDestin[i].setValue(camAniSource[i].value())

        # GEO ANI SOURCE
        geoAniSourceCopyKnobs = ["xform_order","rot_order","translate","rotate","scaling","uniform_scale","skew","pivot"]
        for i in geoAniSourceCopyKnobs:
            geoAniDestin[i].clearAnimated()
            # Copy the animation if it exists
            if geoAniSource[i].isAnimated():
                geoAniDestin[i].copyAnimations(geoAniSource[i].animations())
            # Otherwise just copy the values
            else:
                try:
                    geoAniDestin[i].setValue(geoAniSource[i].getValue())
                except TypeError:
                    geoAniDestin[i].setValue(geoAniSource[i].value())

        # UPDATE CENTERGEO
        # Set inverted transform and rotation order
        centerGeo["xform_order"].setValue(geoAniSource["xform_order"].value()[::-1])
        centerGeo["rot_order"].setValue(geoAniSource["rot_order"].value()[::-1])

def setReferenceFrameToCurrentFrame():
    '''
    Sets the 'reference_frame' knob to the current frame in the active viewer
    '''
    n = nuke.toNode("projectiMate")
    with n as n:
        n['reference_frame'].setValue(nuke.frame())
